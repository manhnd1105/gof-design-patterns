﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoFactory.GangOfFour.Prototype.NETOptimized
{
    class Practice
    {
        public Practice()
        {
            //Setup
            var colorManagement = new ColorManagement() { prototype = new Red() };

            //Run
            colorManagement.CreateExtraRedColor();
        }
    }
    class ColorManagement
    {
        public IPrototype prototype { get; set; }
        public void CreateExtraRedColor()
        {
            IPrototype extraRed = prototype.Clone();
            extraRed.R = 101;
            Console.WriteLine(String.Format("R = {0}, G = {1}, B = {2}", extraRed.R, extraRed.G, extraRed.B));
        }
    }
    interface IPrototype
    {
        int R { get; set; }
        int G { get; set; }
        int B { get; set; }
        IPrototype Clone();
    }
    class Red: IPrototype
    {
        public int R { get; set; }
        public int G { get; set; }
        public int B { get; set; }
        public Red()
        {
            R = 100;
            G = 100;
            B = 100;
        }
        public IPrototype Clone()
        {
            return this;
        }
    }
    class Blue: IPrototype
    {
        public int R { get; set; }
        public int G { get; set; }
        public int B { get; set; }
        public Blue()
        {
            R = 0;
            G = 100;
            B = 100;
        }
        public IPrototype Clone()
        {
            return this;
        }
    }
}
