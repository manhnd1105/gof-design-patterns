﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoFactory.GangOfFour.Mediator.NETOptimized
{
    class Practice
    {
        public Practice()
        {
            //Setup
            IMediator chatRoom = new ChatBus();
            IColleague User1 = new User(chatRoom) {Name = "User1"};
            IColleague User2 = new User(chatRoom) { Name = "User2" };
            IColleague User3 = new Guest(chatRoom) { Name = "User3" };
            chatRoom.Register(User1);
            chatRoom.Register(User2);
            chatRoom.Register(User3);

            //Run
            User1.SendMessage(User2.Name, "Hello world");
            User2.SendMessage(User3.Name, "Hi");
            User3.SendMessage(User2.Name, "Hi " + User2.Name);  
        }
    }
    interface IColleague
    {
        string Name { get; set; }
        IMediator Chatroom { get; set; }
        void SendMessage(string to, string message);
        void ReceiveMessage(string from, string message);
    }
    class User: IColleague
    {
        public string Name { get; set; }
        public IMediator Chatroom { get; set; }
        public User(IMediator chatRoom)
        {
            Chatroom = chatRoom;
        }
        public void SendMessage(string to, string message)
        {
            Chatroom.SendMessage("(User) " + Name, to, message);
        }
        public void ReceiveMessage(string from, string message)
        {
            Console.WriteLine(String.Format("{0} to {1} : {2}", from, Name, message));
        }
    }
    class Guest: IColleague
    {
        public string Name { get; set; }
        public IMediator Chatroom { get; set; }
        public Guest(IMediator chatRoom)
        {
            Chatroom = chatRoom;
        }
        public void SendMessage(string to, string message)
        {
            Chatroom.SendMessage("(Guest) " + Name, to, message);
        }
        public void ReceiveMessage(string from, string message)
        {
            Console.WriteLine(String.Format("{0} to {1} : {2}", from, Name, message));
        }
    }
    interface IMediator
    {
        void SendMessage(string from, string to, string message);
        void Register(IColleague user);
    }
    class ChatBus: IMediator
    {
        List<IColleague> Users { get; set; }
        public ChatBus()
        {
            Users = new List<IColleague>();
        }

        public void SendMessage(string from, string to, string message)
        {
            foreach (var user in Users)
            {
                if (user.Name == to)
                {
                    user.ReceiveMessage(from, message);
                }
            }
            
        }
        public void Register(IColleague user)
        {
            Users.Add(user);
        }
    }
}
