﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoFactory.GangOfFour.Chain.NETOptimized
{
    class Practice
    {
        public Practice()
        {
            //Setup
            var work1 = new UnitOfWork() { Name = "Work 1", PercentageDone = 34, IsApproved = false };
            var work2 = new UnitOfWork() { Name = "Work 2", PercentageDone = 51, IsApproved = false };
            var work3 = new UnitOfWork() { Name = "Work 3", PercentageDone = 100, IsApproved = false };

            IReviewer review2 = new ReviewerLevel2() { Name = "Reviewer 2", Boss = null };
            IReviewer review1 = new ReviewerLevel1() { Name = "Reviewer 1", Boss = review2 };

            //Run
            review1.Approve(work1);
            review1.Approve(work2);
            review1.Approve(work3);
        }
    }

    class UnitOfWork
    {
        public string Name { get; set; }
        public int PercentageDone { get; set; }
        public bool IsApproved { get; set; }
    }

    interface IReviewer
    {
        string Name { get; set; }
        void Approve(UnitOfWork work);
        IReviewer Boss { get; set; }
    }
    class ReviewerLevel1: IReviewer
    {
        public string Name { get; set; }
        public IReviewer Boss { get; set; }
        public void Approve(UnitOfWork work)
        {
            if (work.PercentageDone < 50)
            {
                Console.WriteLine(String.Format("The work {0} is OK. Approver: {1}", work.Name, Name));
                work.IsApproved = true;
            }
            else
            {
                work.IsApproved = false;
                if (Boss != null)
                    Boss.Approve(work);
            }
        }
    }
    class ReviewerLevel2: IReviewer
    {
        public string Name { get; set; }
        public IReviewer Boss { get; set; }
        public void Approve(UnitOfWork work)
        {
            if (work.PercentageDone == 100)
            {
                Console.WriteLine(String.Format("The work {0} is OK. Approver: {1}", work.Name, Name));
                work.IsApproved = true;
            }
            else
            {
                work.IsApproved = false;
                if (Boss != null)
                    Boss.Approve(work);
            }
        }
    }
}
