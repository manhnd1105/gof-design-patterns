﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoFactory.GangOfFour.Observer.NETOptimized
{
    class Practice
    {
        public Practice()
        {
            //Setup
            var order = new Order() { OrderID = 1, TotalAmount = 10 };
            order.Attach(new Datawarehouse());

            //Run
            order.TotalAmount = 11;
            order.OrderID = 2;
            order.OrderID = 2;
        }
        
    }
    interface ISubject
    {
        void Attach(IObserver observer);
        void Detach(IObserver observer);
        void Notify();
        List<IObserver> Observers { get; set; }
    }
    class Order: ISubject
    {
        private decimal totalAmount;
        private int orderID;
        public decimal TotalAmount
        {
            get { return totalAmount; }
            set 
            {
                if (totalAmount != value)
                {
                    totalAmount = value;
                    Notify();
                }
            }
        }
        public int OrderID
        {
            get { return orderID; }
            set
            {
                if (orderID != value)
                {
                    orderID = value;
                    Notify();
                }
            }
        }
        public List<IObserver> Observers { get; set; }
        public Order()
        {
            Observers = new List<IObserver>();
        }
        public void Attach(IObserver observer)
        {
            Observers.Add(observer);
        }
        public void Detach(IObserver observer)
        {
            Observers.Remove(observer);
        }
        public void Notify()
        {
            foreach (var observer in Observers)
            {
                observer.Update(this);
            }
        }
    }
    interface IObserver
    {
        void Update(ISubject order);
    }
    class Datawarehouse: IObserver
    {
        public Order observerState { get; set; }
        public void Update(ISubject order)
        {
            observerState = (Order)order;
            Console.WriteLine(String.Format("OrderID = {0}, TotalAmount = {1}", observerState.OrderID.ToString(), observerState.TotalAmount.ToString()));
        }
    }
}
