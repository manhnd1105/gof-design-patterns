﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoFactory.GangOfFour.Composite.NETOptimized
{
    class Practice
    {
        public Practice()
        {
            //Setup
            var root = new Category(0, "Root category");
            root.Add(new Category(1, "Category 1"));
            root.Add(new Category(2, "Category 2"));
            var category3 = new Category(3, "Category 3");
            category3.Add(new Item(4, "Item 3.1"));
            category3.Add(new Item(5, "Item 3.2"));
            var category4 = new Category(6, "Category 3.3");
            category4.Add(new Item(7, "Item 3.3.1"));
            category3.Add(category4);
            root.Add(category3);

            //Run
            root.Display(0);
        }
    }

    interface ITreeNode
    {
        int Index {get;set;}
        string Name { get; set; }
        void Display(int depth);
    }
    class Category: ITreeNode
    {
        public int Index { get; set; }
        public string Name { get; set; }
        public List<ITreeNode> Children { get; set; }
        public void Add(ITreeNode node)
        {
            Children.Add(node);
        }
        public void Remove(ITreeNode node)
        {
            Children.Remove(node);
        }
        public ITreeNode GetChild(int index)
        {
            foreach (var item in Children)
            {
                if (item.Index == index)
                {
                    return item;
                }
            }
            return null;
        }
        public void Display(int depth)
        {
            Console.WriteLine(new String('-', depth) + Name);
            foreach (var item in Children)
            {
                item.Display(depth + 1);
            }
        }
        public Category(int index, string name)
        {
            Index = index;
            Name = name;
            Children = new List<ITreeNode>();
        }
    }
    class Item: ITreeNode
    {
        public int Index { get; set; }
        public string Name { get; set; }
        public Item(int index, string name)
        {
            Index = index;
            Name = name;
        }
        public void Display(int depth)
        {
            Console.WriteLine(new String('-', depth) + Name);
        }
    }
}
