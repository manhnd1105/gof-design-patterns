﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoFactory.GangOfFour.State.NETOptimized
{
    class Practice
    {
        public Practice()
        {
            //Setup
            var account = new AccountContext(new NormalState());

            //Run
            Console.WriteLine("Current balance: 0");
            Console.WriteLine("Add 13.7");
            account.AddFund(13.7);
            Console.WriteLine("Add 10");
            account.AddFund(10);
        }
    }
    interface IState
    {
        string Name { get; set; }
        void Handle(AccountContext account);
    }
    class AccountContext
    {
        public IState AccountState { get; set; }
        public double Balance { get; set; }
        public AccountContext(IState state)
        {
            AccountState = state;
            Balance = 0;
        }
        public void AddFund(double amount)
        {
            Balance += amount;
            AccountState.Handle(this);
        }
    }
    class NormalState: IState
    {
        public string Name { get; set; }
        public NormalState()
        {
            Name = "Normal state";
        }
        public void Handle(AccountContext account)
        {
            StatusSwitch(account);
            Console.WriteLine(String.Format("Current status: {0}", account.AccountState.Name));
        }
        private void StatusSwitch(AccountContext account)
        {
            if (account.Balance >= 20)
            {
                account.AccountState = new VipState();
            }
        }
    }
    class VipState: IState
    {
        public string Name { get; set; }
        public VipState()
        {
            Name = "Vip state";
        }
        public void Handle(AccountContext account)
        {
            StatusSwitch(account);
            Console.WriteLine(String.Format("Current status: {0}", account.AccountState.Name));
        }
        private void StatusSwitch(AccountContext account)
        {
            if (account.Balance < 20)
            {
                account.AccountState = new NormalState();
            }
        }
    }
}
